p_InitiateChildMenuDataList();

function p_InitiateChildMenuDataList() {
    var oTableChildMenu = $('#dtListChildMenu').DataTable({
        "bPaginate": true,
        "bSort": false,
        "iDisplayLength": 50,
        "processing": true,
        "serverSide": true,
        searching: true,
        "ajax": {
            "url": url + "/system/menu/getChildMenu",
            "method": "POST",
            "data": function (d) {
                d._token = token,
                d.id_parent = $("#id_parent").val() == "" ? 0:$("#id_parent").val()
            }
        },
        columns: [
            { "data": "name", "name": "name", className: 'text-center' },
            { "data": "link", "name": "link", className: 'text-center' },
            { "data": "order", "name": "order", className: 'text-center' },
            { "render": function (data, type, full, meta) {
                return `<i class="${full.icon}"></i>`
            }, className: 'text-center' },
            {
                "render": function (data, type, full, meta) {
                    let res = `<button class="btn btn-info" id="editTombol" data-id="${full.id}" data-toggle="modal"
                                        data-target="#modelId"><i class="fa fa-edit"></i> Edit</button>&nbsp;`;
                    res += `<button class="btn btn-danger" id="hapusTombol" data-id="${full.id}"><i class="fa fa-trash"></i> Hapus</button>&nbsp;`
                    return res;
                }, className: 'text-center'
            },
        ]
    });

    $("#dtListChildMenu").css("width", "100%");
}
