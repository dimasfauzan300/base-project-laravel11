var clsGlobal = new clsGlobalClass();
var url = $("#url").val()
var token = $('input[name=_token]').val()
var selectedIcon = ``

$("#icon").on('change', function (e) {
    selectedIcon = e.icon
});

$(document).ready(function () {
    p_InitiateDataList();
    $('#icon').iconpicker({
        align: 'left',
    });
});

function p_InitiateDataList() {
    var oTable = $('#dtList').DataTable({
        "bPaginate": true,
        "bSort": false,
        "iDisplayLength": 50,
        "processing": true,
        "serverSide": true,
        searching: true,
        "ajax": {
            "url": url + "/system/menu",
            "method": "POST",
            "data": function (d) {
                d._token = token
            }
        },
        columns: [
            { "data": "name", "name": "name", className: 'text-center' },
            { "data": "link", "name": "link", className: 'text-center' },
            { "data": "order", "name": "order", className: 'text-center' },
            { "render": function (data, type, full, meta) {
                return `<i class="${full.icon}"></i>`
            }, className: 'text-center' },
            {
                "render": function (data, type, full, meta) {
                    let res = `<button class="btn btn-info" id="editTombol" data-id="${full.id}" data-toggle="modal"
                                        data-target="#modelId"><i class="fa fa-edit"></i> Edit</button>&nbsp;`;
                    res += `<button class="btn btn-success" id="subMenu" data-id="${full.id}" data-toggle="modal"
                                        data-target="#modalChildMenu"><i class="fa fa-list"></i> Sub Menu</button>&nbsp;`
                    res += `<button class="btn btn-danger" id="hapusTombol" data-id="${full.id}"><i class="fa fa-trash"></i> Hapus</button>&nbsp;`
                    return res;
                }, className: 'text-center'
            },
        ]
    });

    $("#dtList").css("width", "100%");

    $('.dataTables_filter input')
        .unbind('keypress keyup')
        .bind('keypress keyup', function (e) {
            if ($(this).val().length < 3 && e.keyCode != 13) return;
            oTable.fnFilter($(this).val());
        });
}

$(document).on('click', '#subMenu', function (e) {
    let id = $(this).data('id');
    $("#id_parent").val(id);
    var oTableChildMenu = $('#dtListChildMenu').dataTable();
    oTableChildMenu.fnDraw(false);
});


$("#tombol-tambah").on('click', function (e) {
    e.preventDefault();
    $("#name_role").val('');
    $("#name").val('')
    $("#link_web").val('')
    $("#order_menu").val('')
    $("#id_parent").val('')
    icon = ``

});

$("#simpan-data").on('click', function (e) {
    e.preventDefault()
    let id = $("#id").val();
    let data = {
        _token: token,
        name: $("#name").val(),
        link: $("#link_web").val(),
        order: $("#order_menu").val(),
        parent_id: $("#id_parent").val(),
        icon: selectedIcon,
        id: id,
    }
    if (id) {
        $.ajax({
            type: "post",
            url: url + "/system/menu/update",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    $("#modelId").modal('hide')
                    var otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                    var oTableChildMenu = $('#dtListChildMenu').dataTable();
                    oTableChildMenu.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Uppss', 'Error Server !')
            }
        });
    } else {
        $.ajax({
            type: "post",
            url: url + "/system/menu/simpan",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    $("#modelId").modal('hide')
                    var otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                    var oTableChildMenu = $('#dtListChildMenu').dataTable();
                    oTableChildMenu.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Uppss', 'Error Server !')
            }
        });
    }
});

$(document).on('click', "#editTombol", function () {
    let id = $(this).data('id');
    $.ajax({
        type: "get",
        url: url + "/system/menu/getMenu",
        data: {id: id},
        dataType: "json",
        success: function (response) {
            if (response.success) {
                let data = response.data
                $("#name").val(data.name);
                $("#link_web").val(data.link);
                $("#order_menu").val(data.order);
                $("#icon").iconpicker('setIcon', data.icon);
                $("#id").val(data.id);
            } else {
                $.errorMessage('Upps', response.message)
            }
        },
        error: function () {
            $.errorMessage('Uppss', 'Error Server !')
        }
    });
});

$(document).on('click', "#hapusTombol", function (e) {
    e.preventDefault()
    let id = $(this).data('id');
    $.confirmMessage('Alert !', 'Hapus Menu !', 'Ya', function () {
        $.ajax({
            type: "post",
            url: url + "/system/menu/hapus",
            data: {
                _token: token,
                id: id
            },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menghapus Data !')
                    var otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                    var oTableChildMenu = $('#dtListChildMenu').dataTable();
                    oTableChildMenu.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            }
        });
    })
});
