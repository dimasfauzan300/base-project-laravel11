var clsGlobal = new clsGlobalClass();
var url = $("#url").val()
var token = $('input[name=_token]').val()

$(document).ready(function () {
    p_InitiateDataList();
});

function p_InitiateDataList() {
    var oTable = $('#dtList').DataTable({
        "bPaginate": true,
        "bSort": false,
        "iDisplayLength": 50,
        "processing": true,
        "serverSide": true,
        searching: true,
        "ajax": {
            "url": url + "/system/role",
            "method": "POST",
            "data": function (d) {
                d._token = token
            }
        },
        columns: [
            { "data": "name_role", "name": "name_role", className: 'text-center' },
            {
                "render": function (data, type, full, meta) {
                    let res = `<button class="btn btn-info" id="editTombol" data-id="${full.id}" data-toggle="modal"
                                        data-target="#modelId"><i class="fa fa-edit"></i> Edit</button>&nbsp;`;
                    res += `<button class="btn btn-success" id="RoleAksesTombol" data-id="${full.id}" data-toggle="modal"
                                        data-target="#modelRoleAccess"><i class="fa fa-list"></i> Role Akses</button>&nbsp;`
                    res += `<button class="btn btn-danger" id="hapusTombol" data-id="${full.id}"><i class="fa fa-trash"></i> Hapus</button>&nbsp;`
                    return res;
                }, className: 'text-center'
            },
        ]
    });

    $("#dtList").css("width", "100%");

    $('.dataTables_filter input')
        .unbind('keypress keyup')
        .bind('keypress keyup', function (e) {
            if ($(this).val().length < 3 && e.keyCode != 13) return;
            oTable.fnFilter($(this).val());
        });
}


$("#tombol-tambah").on('click', function (e) {
    e.preventDefault();
    $("#name_role").val('');
});

$("#simpan-data").on('click', function (e) {
    e.preventDefault()
    let id = $("#id").val();
    let data = {
        _token: token,
        name_role: $("#name_role").val(),
        id: id,
    }
    if (id) {
        $.ajax({
            type: "post",
            url: url + "/system/role/update",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    $("#modelId").modal('hide')
                    var otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Uppss', 'Error Server !')
            }
        });
    } else {
        $.ajax({
            type: "post",
            url: url + "/system/role/simpan",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    $("#modelId").modal('hide')
                    var otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Uppss', 'Error Server !')
            }
        });
    }
});

$(document).on('click', "#editTombol", function () {
    let id = $(this).data('id');
    $.ajax({
        type: "get",
        url: url + "/system/role/getRole",
        data: {id: id},
        dataType: "json",
        success: function (response) {
            if (response.success) {
                let data = response.data
                $("#name_role").val(data.name_role);
                $("#id").val(data.id);
            } else {
                $.errorMessage('Upps', response.message)
            }
        },
        error: function () {
            $.errorMessage('Uppss', 'Error Server !')
        }
    });
});

$(document).on('click', "#hapusTombol", function (e) {
    e.preventDefault()
    let id = $(this).data('id');
    $.confirmMessage('Alert !', 'Hapus Role !', 'Ya', function () {
        $.ajax({
            type: "post",
            url: url + "/system/role/hapus",
            data: {
                _token: token,
                id: id
            },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menghapus Data !')
                    let otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            }
        });
    })
});
