p_InitiateDataListRoleAkses();
getAllMenu();

function p_InitiateDataListRoleAkses() {
    var oTableUserRole = $('#dtListRoleAccess').DataTable({
        "bPaginate": true,
        "bSort": false,
        "iDisplayLength": 50,
        "processing": true,
        "serverSide": true,
        searching: true,
        "ajax": {
            "url": url + "/system/role_access",
            "method": "POST",
            "data": function (d) {
                d._token = token,
                d.role_id = $("#role_id").val() == "" ? 0:$("#role_id").val()
            }
        },
        columns: [
            { "data": "name", "name": "name", className: 'text-center' },
            {
                "render": function (data, type, full, meta) {
                    let res = ``;
                    res += `<button class="btn btn-danger" id="hapusTombolRoleAksses" data-id="${full.id}"><i class="fa fa-trash"></i> Hapus</button>&nbsp;`
                    return res;
                }, className: 'text-center'
            },
        ]
    });
}

function getAllMenu () {
    $.ajax({
        type: "get",
        url: url + "/system/menu/getAllMenu",
        dataType: "json",
        success: function (response) {
            if (response.success) {
                let data = response.data
                let htmlOption = `<option value="">Select Menu</option>`
                $.each(data, function (i, item) {
                    htmlOption += `<option value="${item.id}">${item.name}</option>`
                });
                $("#menu_id").html(htmlOption);
            } else {
                $.errorMessage('Upps !', response.message)
            }
        },
        error: function () {
            $.errorMessage('Upps !', 'Server Error !')
        }
    });
}

$(document).on('click', "#RoleAksesTombol", function (e) {
    e.preventDefault();
    let id = $(this).data('id');
    $("#role_id").val(id);
    var oTableUserRole = $('#dtListRoleAccess').dataTable();
    oTableUserRole.fnDraw(false);
});


$(document).on('click', "#hapusTombolRoleAksses", function (e) {
    e.preventDefault()
    let id = $(this).data('id');
    $.confirmMessage('Alert !', 'Hapus Akses Role !', 'Ya', function () {
        $.ajax({
            type: "post",
            url: url + "/system/role_access/hapus",
            data: {
                _token: token,
                id: id
            },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menghapus Data !')
                    var oTableUserRole = $('#dtListAksesRole').dataTable();
                    oTableUserRole.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Upps !', 'Server Error !')
            }
        });
    })
});

$("#tombol-tambah-roleakses").on('click', function () {
    $("#menu_id").val('')
});

$("#simpan-data-roleakses").on('click', function (e) {
    e.preventDefault()
    let data = {
        _token: token,
        role_id: $("#role_id").val(),
        menu_id: $("#menu_id").val(),
    }
    $.ajax({
        type: "post",
        url: url + "/system/role_access/simpan",
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    $("#modelAddRoleAccess").modal('hide')
                    var oTableUserRole = $('#dtListRoleAccess').dataTable();
                    oTableUserRole.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
        },
        error: function () {
            $.errorMessage('Upps !', 'Server Error !')
        }
    });
});
