p_InitiateDataListUserRole();
getAllRole();

function p_InitiateDataListUserRole() {
    var oTableUserRole = $('#dtListUserRole').DataTable({
        "bPaginate": true,
        "bSort": false,
        "iDisplayLength": 50,
        "processing": true,
        "serverSide": true,
        searching: true,
        "ajax": {
            "url": url + "/system/user_role",
            "method": "POST",
            "data": function (d) {
                d._token = token,
                d.user_id = $("#user_id").val() == "" ? 0:$("#user_id").val()
            }
        },
        columns: [
            { "data": "name_role", "name_role": "name", className: 'text-center' },
            {
                "render": function (data, type, full, meta) {
                    let res = ``;
                    res += `<button class="btn btn-danger" id="hapusTombolUserRole" data-id="${full.id}"><i class="fa fa-trash"></i> Hapus</button>&nbsp;`
                    return res;
                }, className: 'text-center'
            },
        ]
    });
}

function getAllRole () {
    $.ajax({
        type: "get",
        url: url + "/system/role/getAllRole",
        dataType: "json",
        success: function (response) {
            if (response.success) {
                let data = response.data
                let htmlOption = `<option value="">Select Role</option>`
                $.each(data, function (i, item) {
                    htmlOption += `<option value="${item.id}">${item.name_role}</option>`
                });
                $("#role_id").html(htmlOption);
            } else {
                $.errorMessage('Upps !', response.message)
            }
        },
        error: function () {
            $.errorMessage('Upps !', 'Server Error !')
        }
    });
}

$(document).on('click', "#roleTombol", function (e) {
    e.preventDefault();
    let id = $(this).data('id');
    $("#user_id").val(id);
    var oTableUserRole = $('#dtListUserRole').dataTable();
    oTableUserRole.fnDraw(false);
});


$(document).on('click', "#hapusTombolUserRole", function (e) {
    e.preventDefault()
    let id = $(this).data('id');
    $.confirmMessage('Alert !', 'Hapus Role User !', 'Ya', function () {
        $.ajax({
            type: "post",
            url: url + "/system/user_role/hapus",
            data: {
                _token: token,
                id: id
            },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menghapus Data !')
                    var oTableUserRole = $('#dtListUserRole').dataTable();
                    oTableUserRole.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Upps !', 'Server Error !')
            }
        });
    })
});

$("#tombol-tambah-userrole").on('click', function () {
    $("#role_id").val('')
});

$("#simpan-data-userrole").on('click', function (e) {
    e.preventDefault()
    let data = {
        _token: token,
        user_id: $("#user_id").val(),
        role_id: $("#role_id").val(),
    }
    $.ajax({
        type: "post",
        url: url + "/system/user_role/simpan",
        data: data,
        dataType: "json",
        success: function (response) {
            if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    $("#modelAddUserRole").modal('hide')
                    var oTableUserRole = $('#dtListUserRole').dataTable();
                    oTableUserRole.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
        },
        error: function () {
            $.errorMessage('Upps !', 'Server Error !')
        }
    });
});
