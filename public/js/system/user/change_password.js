var clsGlobal = new clsGlobalClass();
var url = $("#url").val()
var token = $('input[name=_token]').val()

$("#simpan_password").on('click', function (e) {
    e.preventDefault()
    let password = $("#new_password").val()
    let password_confirm = $("#confirm_password").val()

    if (password != password_confirm) {
        $.errorMessage('Uppss', 'Confirmation Password Salah !')
        return
    }

    let data = {
        _token: token,
        password: password,
    }
    $.ajax({
            type: "post",
            url: url + "/system/user/change_password",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    window.location.reload()
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Uppss', 'Error Server !')
            }
        });
});
