var clsGlobal = new clsGlobalClass();
var url = $("#url").val()
var token = $('input[name=_token]').val()

$(document).ready(function () {
    p_InitiateDataList();
});

function p_InitiateDataList() {
    var oTable = $('#dtList').DataTable({
        "bPaginate": true,
        "bSort": false,
        "iDisplayLength": 50,
        "processing": true,
        "serverSide": true,
        searching: true,
        "ajax": {
            "url": url + "/system/user",
            "method": "POST",
            "data": function (d) {
                d._token = token
            }
        },
        columns: [
            { "data": "name", "name": "name", className: 'text-center' },
            { "data": "nik", "name": "nik", className: 'text-center' },
            { "data": "email", "name": "email", className: 'text-center' },
            {
                "render": function (data, type, full, meta) {
                    let res = `<button class="btn btn-info" id="editTombol" data-id="${full.id}" data-toggle="modal"
                                        data-target="#modelId"><i class="fa fa-edit"></i> Edit</button>&nbsp;`;
                    res += `<button class="btn btn-success" id="roleTombol" data-id="${full.id}" data-toggle="modal"
                                        data-target="#modelUserRole"><i class="fa fa-user"></i> Role</button>&nbsp;`
                    res += `<button class="btn btn-warning" id="changePasswordTombol" data-id="${full.id}"><i class="fa fa-key"></i> Reset Password</button>&nbsp;`
                    res += `<button class="btn btn-danger" id="hapusTombol" data-id="${full.id}"><i class="fa fa-trash"></i> Hapus</button>&nbsp;`
                    return res;
                }, className: 'text-center'
            },
        ]
    });

    $("#dtList").css("width", "100%");

    $('.dataTables_filter input')
        .unbind('keypress keyup')
        .bind('keypress keyup', function (e) {
            if ($(this).val().length < 3 && e.keyCode != 13) return;
            oTable.fnFilter($(this).val());
        });
}


$("#tombol-tambah").on('click', function (e) {
    e.preventDefault();
    $("#name").val('');
    $("#nik").val('');
    $("#id").val('');
    $("#email").val('');
});

$("#simpan-data").on('click', function (e) {
    e.preventDefault()
    let id = $("#id").val();
    let data = {
        _token: token,
        name: $("#name").val(),
        nik: $("#nik").val(),
        email: $("#email").val(),
        id: id,
    }
    if (id) {
        $.ajax({
            type: "post",
            url: url + "/system/user/update",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    $("#modelId").modal('hide')
                    var otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Uppss', 'Error Server !')
            }
        });
    } else {
        $.ajax({
            type: "post",
            url: url + "/system/user/simpan",
            data: data,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menyimpan Data !')
                    $("#modelId").modal('hide')
                    var otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            },
            error: function () {
                $.errorMessage('Uppss', 'Error Server !')
            }
        });
    }
});

$(document).on('click', "#editTombol", function () {
    let id = $(this).data('id');
    $.ajax({
        type: "get",
        url: url + "/system/user/getUser",
        data: {id: id},
        dataType: "json",
        success: function (response) {
            if (response.success) {
                let data = response.data
                $("#name").val(data.name);
                $("#nik").val(data.nik);
                $("#email").val(data.email);
                $("#id").val(data.id);
            } else {
                $.errorMessage(response.message, response.stack_trace)
            }
        },
        error: function () {
            $.errorMessage('Uppss', 'Error Server !')
        }
    });
});

$(document).on('click', "#changePasswordTombol", function (e) {
    e.preventDefault()
    let id = $(this).data('id');
    $.confirmMessage('Alert !', 'Password akan dirubah menjadi default', 'Ya', function ()  {
        $.ajax({
            type: "post",
            url: url + "/system/user/reset_password",
            data: {
                _token: token,
                id: id
            },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Reset Password !')
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            }
        });
    })
});

$(document).on('click', "#hapusTombol", function (e) {
    e.preventDefault()
    let id = $(this).data('id');
    $.confirmMessage('Alert !', 'Hapus User !', 'Ya', function () {
        $.ajax({
            type: "post",
            url: url + "/system/user/hapus",
            data: {
                _token: token,
                id: id
            },
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $.successMessage('Sukses !', 'Sukses Menghapus Data !')
                    let otable = $('#dtList').dataTable();
                    otable.fnDraw(false);
                } else {
                    $.errorMessage(response.message, response.stack_trace)
                }
            }
        });
    })
});
