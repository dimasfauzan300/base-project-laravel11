@php
    use App\Helpers\SessionsHelper;
    use App\Helpers\MenuHelper;
@endphp
<ul class="nav nav-treeview">
    @foreach ($menus as $menu)
        @php
            $child_menu = $menu_helper->getChildMenu($menu->id);
        @endphp
        @if (count($child_menu) > 0)
            <li class="nav-item has-treeview">
                <a href="#" class="nav-link">
                    <i class="nav-icon {{ $menu->icon }}"></i>
                    <p>
                        {{ $menu->name }}
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                @include('layouts.submenu', ['menus' => $child_menu])
            </li>
        @else
            <li class="nav-item">
                <a href="{{ url($menu->link) }}" class="nav-link">
                    <i class="{{ $menu->icon }} nav-icon"></i>
                    <p>{{ $menu->name }}</p>
                </a>
            </li>
        @endif
    @endforeach

</ul>
