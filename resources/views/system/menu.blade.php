@section('custom_css')
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" />
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/dist/sweetalert2.min.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets/plugins/bootstrap-iconpicker-1.10.0/dist/css/bootstrap-iconpicker.min.css') }}">
@endsection
@extends('layouts.layout')
@section('content')
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Data Menu</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">System</li>
                            <li class="breadcrumb-item active">Menu</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12 mb-3 d-flex justify-content-end">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary btn-md" data-toggle="modal"
                                            data-target="#modelId" id="tombol-tambah">
                                            <i class="fa fa-plus"></i> Tambah Data Menu
                                        </button>
                                    </div>

                                </div>
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table class="table table-bordered" id="dtList" style="width: 100%">
                                            <thead>
                                                <tr>
                                                    <th class="text-center">Nama</th>
                                                    <th class="text-center">Link</th>
                                                    <th class="text-center">Order</th>
                                                    <th class="text-center">Icon</th>
                                                    <th class="text-center">Option</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.card -->
                </div>
                <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Modal -->
    <div class="modal fade" id="modalChildMenu" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog modal-xl" style="max-width: 90%;" role="document">
            <form action="#" id="data-formulir">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Data Sub Menu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="state" name="state" value="">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="id_parent" id="id_parent">
                            </div>
                            <div class="col-lg-12 mb-3 d-flex justify-content-end">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary btn-md" data-toggle="modal"
                                    data-target="#modelId" id="tombol-tambah-submenu">
                                    <i class="fa fa-plus"></i> Tambah Data Menu
                                </button>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dtListChildMenu" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Nama</th>
                                                <th class="text-center">Link</th>
                                                <th class="text-center">Order</th>
                                                <th class="text-center">Icon</th>
                                                <th class="text-center">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="#" id="data-formulir">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Data Menu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="state" name="state" value="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="jenis_menu">Nama</label>
                                    <input type="text" id="name" name="name" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="jenis_menu">Link</label>
                                    <input type="text" id="link_web" name="link" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="jenis_menu">Order</label>
                                    <input type="text" id="order_menu" name="order_menu" class="form-control"
                                        value="">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2 col-xs-6">
                                            <label for="">Icon</label>
                                        </div>
                                        <div class="col-lg-10 col-xs-6">
                                            <div data-iconset="font-awesome" id="icon"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary" id="simpan-data">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script_custom')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bootstrap-iconpicker-1.10.0/dist/js/bootstrap-iconpicker.bundle.min.js') }}">
    </script>
    <script src="{{ asset('js/system/menu/menu.js') }}"></script>
    <script src="{{ asset('js/system/menu/submenu.js') }}"></script>
@endsection
