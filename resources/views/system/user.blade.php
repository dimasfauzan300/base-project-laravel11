@section('custom_css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/dist/sweetalert2.min.css') }}">
@endsection
@extends('layouts.layout')
@section('content')
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Data User</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">System</li>
                            <li class="breadcrumb-item active">User</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                        <div class="card card-primary card-outline">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-12 mb-3 d-flex justify-content-end">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary btn-md" data-toggle="modal"
                                            data-target="#modelId" id="tombol-tambah">
                                            <i class="fa fa-plus"></i> Tambah Data
                                        </button>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="table-responsive">
                                            <table class="table table-bordered" id="dtList" style="width: 100%">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">Nama</th>
                                                        <th class="text-center">NIK</th>
                                                        <th class="text-center">Email</th>
                                                        <th class="text-center">Option</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.card -->
                    </div>
                    <!-- /.col-md-6 -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <!-- Modal -->
    <div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="#" id="data-formulir">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Data User</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="state" name="state" value="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="jenis_menu">Nama</label>
                                    <input type="text" id="name" name="name" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="jenis_menu">NIK</label>
                                    <input type="text" id="nik" name="nik" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="jenis_menu">Email</label>
                                    <input type="email" id="email" name="email" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="id" id="id" value="">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary" id="simpan-data">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="modelUserRole" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 80%;" role="document">
            <form action="#" id="data-formulir">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Data User Role</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="user_id" name="user_id" value="">
                        <div class="row">
                            <div class="col-lg-12 mb-3 d-flex justify-content-end">
                                        <!-- Button trigger modal -->
                                        <button type="button" class="btn btn-primary btn-md" data-toggle="modal"
                                            data-target="#modelAddUserRole" id="tombol-tambah-userrole">
                                            <i class="fa fa-plus"></i> Tambah Data Role
                                        </button>
                                    </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="dtListUserRole" style="width: 100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">Role</th>
                                                <th class="text-center">Option</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="modelAddUserRole" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form action="#" id="data-formulir">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Data User Role</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="state" name="state" value="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="jenis_menu">Role</label>
                                    <select name="" id="role_id" class="form-control"></select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button class="btn btn-primary" id="simpan-data-userrole">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script_custom')
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/system/user/user.js') }}"></script>
    <script src="{{ asset('js/system/user/user_role.js') }}"></script>
@endsection
