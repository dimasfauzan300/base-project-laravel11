<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Model;

interface AuthServices
{
    public function login(string $username, string $password) : Model;
}
