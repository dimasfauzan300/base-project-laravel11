<?php

namespace App\Services;

use App\Helpers\ApiHelper;
use Illuminate\Http\JsonResponse;

interface UserRoleServices
{
    public function getDataTable(int $id_user) : JsonResponse;
    public function save(array $data) : ApiHelper;
    public function delete(int $id) : ApiHelper;
}
