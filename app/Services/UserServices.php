<?php

namespace App\Services;

use App\Helpers\ApiHelper;
use Illuminate\Http\JsonResponse;

interface UserServices
{
    public function getDataTable() : JsonResponse;
    public function getUser(int $id) : ApiHelper;
    public function save(array $data) : ApiHelper;
    public function update(int $id, array $data) : ApiHelper;
    public function delete(int $id) : ApiHelper;
}
