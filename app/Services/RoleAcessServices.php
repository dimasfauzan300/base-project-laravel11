<?php

namespace App\Services;

use App\Helpers\ApiHelper;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;

interface RoleAcessServices
{
    public function cekRoleUser(int $id_user) : Collection;
    public function getDataTable(int $role_id) : JsonResponse;
    public function save(array $data) : ApiHelper;
    public function delete(int $id) : ApiHelper;
}
