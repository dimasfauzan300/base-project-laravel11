<?php

namespace App\Services;

use App\Helpers\ApiHelper;
use Illuminate\Http\JsonResponse;

interface MenuServices
{
    public function getDataTable() : JsonResponse;
    public function getMenuId(int $id) : ApiHelper;
    public function save(array $data) : ApiHelper;
    public function update(int $id, array $data) : ApiHelper;
    public function delete(int $id) : ApiHelper;

    public function getChildMenuByParentID (int $id_parent) : JsonResponse;
    public function getAllMenu () : ApiHelper;
}
