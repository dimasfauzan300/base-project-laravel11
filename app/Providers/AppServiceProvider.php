<?php

namespace App\Providers;

use App\BuisnessLogic\AuthBuisnessLogic;
use App\BuisnessLogic\MenuBuisnessLogic;
use App\BuisnessLogic\RoleAccessBuisnessLogic;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\System\RoleAccessController;
use App\Services\AuthServices;
use App\Services\MenuServices;
use App\Services\RoleAcessServices;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        // Register services class disini, gunakan sesuai controller yang dibutuhkan untuk menghemat memori
        $this->app->bind(MenuServices::class, MenuBuisnessLogic::class);
        $this->app->when(AuthController::class)
            ->needs(AuthServices::class)
            ->give(function () {
                return new AuthBuisnessLogic();
            });
        $this->app->when([AuthController::class, RoleAccessController::class])
            ->needs(RoleAcessServices::class)
            ->give(function () {
                return new RoleAccessBuisnessLogic();
            });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {

    }
}
