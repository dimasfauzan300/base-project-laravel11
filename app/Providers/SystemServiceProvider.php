<?php

namespace App\Providers;

use App\BuisnessLogic\RoleBuisnessLogic;
use App\BuisnessLogic\UserBuissnessLogic;
use App\BuisnessLogic\UserRoleBuisnessLogic;
use App\Http\Controllers\System\RoleController;
use App\Http\Controllers\System\UserController;
use App\Http\Controllers\System\UserRoleController;
use App\Services\RoleServices;
use App\Services\UserRoleServices;
use App\Services\UserServices;
use Illuminate\Support\ServiceProvider;

class SystemServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->when([UserController::class])
            ->needs(UserServices::class)
            ->give(function () {
                return new UserBuissnessLogic();
            });
        $this->app->when([RoleController::class])
            ->needs(RoleServices::class)
            ->give(function () {
                return new RoleBuisnessLogic();
            });
        $this->app->when([UserRoleController::class])
            ->needs(UserRoleServices::class)
            ->give(function () {
                return new UserRoleBuisnessLogic();
            });
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
