<?php

namespace App\BuisnessLogic;

use App\Helpers\ApiHelper;
use App\Models\Role;
use App\Services\RoleServices;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

final class RoleBuisnessLogic implements RoleServices
{
    public function getDataTable() : JsonResponse
    {
        $data = Role::orderBy('id');
        return DataTables()->of($data)->make(true);
    }

    public function getRole (int $id) : ApiHelper
    {
        try {
            $data = Role::where('id', $id)->first();
            return ApiHelper::CreateResult(true, $data, "");
        } catch (Exception $e) {
            return ApiHelper::CreateError($e);
        }
    }

    public function getAllRole () : ApiHelper
    {
        try {
            $data = Role::get();
            return ApiHelper::CreateResult(true, $data, "");
        } catch (Exception $e) {
            return ApiHelper::CreateError($e);
        }
    }

    public function save(array $data) : ApiHelper
    {
        DB::beginTransaction();
        try {
            $exist = Role::where('name_role', $data['name_role'])->exists();
            if ($exist) {
                return ApiHelper::CreateResult(false, [], "Data Sudah Ada !");
            }
            Role::insert($data);
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function update(int $id, array $data) : ApiHelper
    {
        DB::beginTransaction();
        try {
            Role::where('id', $id)->update($data);
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function delete(int $id) : ApiHelper
    {
        DB::beginTransaction();
        try {
            Role::where('id', $id)->delete();
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }
}
