<?php

namespace App\BuisnessLogic;

use App\Helpers\ApiHelper;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use App\Services\UserServices;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

final class UserBuissnessLogic implements UserServices
{
    public function getUser(int $id) : ApiHelper
    {
        try {
            $data = User::where('id', $id)->first();
            return ApiHelper::CreateResult(true, $data, "");
        } catch (Exception $e) {
            return ApiHelper::CreateError($e);
        }
    }

    public function getDataTable() : JsonResponse
    {
        $query = User::orderBy('id');
        return DataTables()->of($query)->make(true);
    }

    public function save(array $data) : ApiHelper
    {
        DB::beginTransaction();
        try {
            $exist = User::where([
                'email' => $data["email"],
                'name' => $data["name"]
            ])->exists();
            if ($exist) {
                return ApiHelper::CreateResult(false, [], "Data Sudah Ada !");
            }
            User::insert($data);

            //Asign Role To Guest
            $dataInsert = User::where([
                'email' => $data["email"],
                'name' => $data["name"]
            ])->first();
            $dataRole = Role::where('name_role', 'Guest')->first();
            UserRole::insert([
                'user_id' => $dataInsert->id,
                'role_id' => $dataRole->id
            ]);

            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function update(int $id, array $data) : ApiHelper
    {
        DB::beginTransaction();
        try {
            User::where('id', $id)->update($data);
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function delete(int $id) : ApiHelper
    {
        DB::beginTransaction();
        try {
            User::where('id', $id)->delete();
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }
}
