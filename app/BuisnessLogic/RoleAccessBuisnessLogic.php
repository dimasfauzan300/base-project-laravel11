<?php

namespace App\BuisnessLogic;

use App\Helpers\ApiHelper;
use App\Models\Menu;
use App\Models\RoleAccess;
use App\Models\UserRole;
use App\Services\RoleAcessServices;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

final class RoleAccessBuisnessLogic implements RoleAcessServices
{
    public function cekRoleUser(int $id_user) : Collection
    {
        $data_role = UserRole::join('roles', 'userrole.role_id', '=', 'roles.id')
            ->where([
                'user_id' => $id_user
            ])->get();
        return $data_role;
    }

    public function cekAccessRole(string $path, int $id_role) : bool
    {
        $AccessRole = Menu::join('roleaccess', 'menus.id', '=', 'roleaccess.menu_id')
            ->where([
                'link' => $path,
                'role_id' => $id_role
            ])->exists();
        return $AccessRole;
    }

    public function getDataTable(int $role_id) : JsonResponse
    {
        $data = RoleAccess::join('menus', 'roleaccess.menu_id', '=', 'menus.id')
            ->where('role_id', $role_id)
            ->select(['roleaccess.*', 'menus.name']);
        return DataTables()->of($data)->make(true);
    }

    public function save(array $data) : ApiHelper
    {
        DB::beginTransaction();
        try {
            $exist = RoleAccess::where([
                'role_id' => $data['role_id'],
                'menu_id' => $data['menu_id']
            ])->exists();
            if ($exist) {
                return ApiHelper::CreateResult(false, [], "Data Sudah Ada !");
            }
            RoleAccess::insert($data);
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function delete(int $id) : ApiHelper
    {
        DB::beginTransaction();
        try {
            RoleAccess::where('id', $id)->delete();
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }
}
