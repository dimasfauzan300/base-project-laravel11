<?php

namespace App\BuisnessLogic;

use App\Helpers\ApiHelper;
use App\Models\UserRole;
use App\Services\UserRoleServices;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

final class UserRoleBuisnessLogic implements UserRoleServices
{
    public function getDataTable(int $id_user) : JsonResponse
    {
        $data = UserRole::join('roles', 'userrole.role_id', '=', 'roles.id')
            ->where('user_id', $id_user)
            ->select(['userrole.*', 'roles.name_role']);
        return DataTables()->of($data)->make(true);
    }

    public function save(array $data) : ApiHelper
    {
        DB::beginTransaction();
        try {
            $exist = UserRole::where([
                'role_id' => $data['role_id'],
                'user_id' => $data['user_id']
            ])->exists();
            if ($exist) {
                return ApiHelper::CreateResult(false, [], "Data Sudah Ada !");
            }
            UserRole::insert($data);
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function delete(int $id) : ApiHelper
    {
        DB::beginTransaction();
        try {
            UserRole::where('id', $id)->delete();
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }
}
