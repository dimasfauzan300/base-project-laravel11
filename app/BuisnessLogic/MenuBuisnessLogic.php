<?php

namespace App\BuisnessLogic;

use App\Helpers\ApiHelper;
use App\Models\Menu;
use App\Services\MenuServices;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;

final class MenuBuisnessLogic implements MenuServices
{
    public function getMenu($role_id) : Collection
    {
        $menus = Menu::join('roleaccess', 'menus.id', '=', 'roleaccess.menu_id')
            ->where([
                "roleaccess.role_id" => $role_id,
            ])
            ->whereNull('menus.parent_id')
            ->orderBy('menus.order')
            ->get();
        return $menus;
    }

    public function getChildMenu($role_id, $parent_id) : Collection
    {
        $menus = Menu::join('roleaccess', 'menus.id', '=', 'roleaccess.menu_id')
            ->where([
                "roleaccess.role_id" => $role_id,
                'menus.parent_id' => $parent_id
            ])
            ->orderBy('menus.order')
            ->get();
        return $menus;
    }

    public function getMenuId (int $id) : ApiHelper
    {
        try {
            $data = Menu::where('id', $id)->first();
            return ApiHelper::CreateResult(true, $data, "");
        } catch (Exception $e) {
            return ApiHelper::CreateError($e);
        }
    }

    public function getDataTable() : JsonResponse
    {
        $query = Menu::orderBy('order')->whereNull('parent_id');
        return DataTables()->of($query)->make(true);
    }

    public function save (array $data) : ApiHelper
    {
        DB::beginTransaction();
        try {
            $exist = Menu::where('name', $data['name'])->exists();
            if ($exist) {
                return ApiHelper::CreateResult(false, [], "Data Sudah Ada !");
            }
            Menu::insert($data);
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function update (int $id, array $data) : ApiHelper
    {
        DB::beginTransaction();
        try {
            Menu::where('id', $id)->update($data);
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function delete (int $id) : ApiHelper
    {
        DB::beginTransaction();
        try {
            Menu::where('id', $id)->delete();
            DB::commit();
            return ApiHelper::CreateResult(true, [], "Data Tersimpan !");
        } catch (Exception $e) {
            DB::rollBack();
            return ApiHelper::CreateError($e);
        }
    }

    public function getChildMenuByParentID (int $id_parent): JsonResponse
    {
        $query = Menu::where('parent_id', $id_parent);
        return DataTables()->of($query)->make(true);
    }

    public function getAllMenu() : ApiHelper
    {
        try {
            $data = Menu::get();
            return ApiHelper::CreateResult(true, $data, "");
        } catch (Exception $e) {
            return ApiHelper::CreateError($e);
        }
    }
}
