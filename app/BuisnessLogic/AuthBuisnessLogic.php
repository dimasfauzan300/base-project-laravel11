<?php

namespace App\BuisnessLogic;

use App\Models\User;
use App\Services\AuthServices;
use Illuminate\Database\Eloquent\Model;

final class AuthBuisnessLogic implements AuthServices
{
    public function login(string $email, string $password) : Model
    {
        $isValid = User::where([
                'email' => $email
            ])->first();
        return $isValid;
    }
}
