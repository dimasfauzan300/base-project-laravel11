<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleAccess extends Model
{
    use HasFactory;

    protected $table = "roleaccess";
    protected $primaryKey = 'id';

    public function menus()
    {
        return $this->belongsToMany(Menu::class);
    }
}
