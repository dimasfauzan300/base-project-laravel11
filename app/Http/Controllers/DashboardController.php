<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class DashboardController extends Controller
{
    public function index() : View
    {
        $data['title'] = 'Home Page';
        return view('Dashboard.home', $data);
    }
}
