<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Services\RoleAcessServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoleAccessController extends Controller
{
    private $roleAccessService;

    public function __construct(RoleAcessServices $roleAccessService)
    {
        $this->roleAccessService = $roleAccessService;
    }

    public function getDataTable (Request $request) : JsonResponse {
        $data = $this->roleAccessService->getDataTable($request->input('role_id'));
        return $data;
    }

    public function simpan (Request $request) : JsonResponse {
        $data = $this->roleAccessService->save([
            'role_id' => $request->input('role_id'),
            'menu_id' => $request->input('menu_id'),
        ]);
        return response()->json($data, 200);
    }

    public function hapus (Request $request) : JsonResponse {
        $data = $this->roleAccessService->delete($request->input('id'));
        return response()->json($data, 200);
    }
}
