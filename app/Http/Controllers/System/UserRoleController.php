<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Services\UserRoleServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserRoleController extends Controller
{
    private $userRoleService;

    public function __construct(UserRoleServices $userRoleService)
    {
        $this->userRoleService = $userRoleService;
    }

    public function getDataTable (Request $request) : JsonResponse {
        $data = $this->userRoleService->getDataTable($request->input('user_id'));
        return $data;
    }

    public function simpan (Request $request) : JsonResponse {
        $data = $this->userRoleService->save([
            'role_id' => $request->input('role_id'),
            'user_id' => $request->input('user_id'),
        ]);
        return response()->json($data, 200);
    }

    public function hapus (Request $request) : JsonResponse {
        $data = $this->userRoleService->delete($request->input('id'));
        return response()->json($data, 200);
    }
}
