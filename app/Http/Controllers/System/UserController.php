<?php

namespace App\Http\Controllers\System;

use App\Helpers\SessionsHelper;
use App\Http\Controllers\Controller;
use App\Services\UserServices;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\View\View;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserServices $user)
    {
        $this->userService = $user;
    }

    public function index () : View {
        $data['title'] = 'Data User';
        return view('system.user', $data);
    }

    public function formChangePassword () : View {
        $data['title'] = 'Form Ubah Password';
        return view('system.form_change_password', $data);
    }

    public function getDataTable () : JsonResponse {
        $data = $this->userService->getDataTable();
        return $data;
    }

    public function getUser (Request $request) {
        $data = $this->userService->getUser($request->input('id'));
        return response()->json($data);
    }

    public function simpan (Request $request) : JsonResponse {
        $data = $this->userService->save([
            'name' => $request->input('name'),
            'nik' => $request->input('nik'),
            'email' => $request->input('email'),
            'password' => Hash::make('default'),
        ]);
        return response()->json($data, 200);
    }

    public function update (Request $request) : JsonResponse {
        $data = $this->userService->update($request->input('id'), [
            'name' => $request->input('name'),
            'nik' => $request->input('nik'),
            'email' => $request->input('email'),
        ]);
        return response()->json($data, 200);
    }

    public function changePassword (Request $request) : JsonResponse {
        $data = $this->userService->update($request->input('id'), [
            'password' => Hash::make($request->input('password')),
        ]);
        return response()->json($data, 200);
    }

    public function changeMyPassword (Request $request) : JsonResponse {
        $data = $this->userService->update(SessionsHelper::getSessions()->id, [
            'password' => Hash::make($request->input('password')),
        ]);
        return response()->json($data, 200);
    }

    public function hapus (Request $request) : JsonResponse {
        $data = $this->userService->delete($request->input('id'));
        return response()->json($data, 200);
    }
}
