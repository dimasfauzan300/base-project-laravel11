<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Services\RoleServices;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    private $roleService;

    public function __construct(RoleServices $user)
    {
        $this->roleService = $user;
    }

    public function index () : View {
        $data['title'] = 'Data Role';
        return view('system.role', $data);
    }

    public function getDataTable () : JsonResponse {
        $data = $this->roleService->getDataTable();
        return $data;
    }

    public function getRole (Request $request) {
        $data = $this->roleService->getRole($request->input('id'));
        return response()->json($data);
    }

    public function getAllRole () {
        $data = $this->roleService->getAllRole();
        return response()->json($data);
    }

    public function simpan (Request $request) : JsonResponse {
        $data = $this->roleService->save([
            'name_role' => $request->input('name_role'),
        ]);
        return response()->json($data, 200);
    }

    public function update (Request $request) : JsonResponse {
        $data = $this->roleService->update($request->input('id'), [
            'name_role' => $request->input('name_role'),
        ]);
        return response()->json($data, 200);
    }

    public function hapus (Request $request) : JsonResponse {
        $data = $this->roleService->delete($request->input('id'));
        return response()->json($data, 200);
    }
}
