<?php

namespace App\Http\Controllers\System;

use App\Http\Controllers\Controller;
use App\Services\MenuServices;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    private $menuServices;

    public function __construct(MenuServices $menuServices) {
        $this->menuServices = $menuServices;
    }

    public function index () : View {
        $data['title'] = 'Data Menu';
        return view('system.menu', $data);
    }

    public function getDataTable () : JsonResponse {
        $data = $this->menuServices->getDataTable();
        return $data;
    }

    public function getMenu (Request $request) : JsonResponse {
        $data = $this->menuServices->getMenuId($request->input('id'));
        return response()->json($data);
    }

    public function simpan (Request $request) : JsonResponse {
        $data = $this->menuServices->save([
            'name' => $request->input('name'),
            'link' => $request->input('link'),
            'parent_id' => $request->input('parent_id'),
            'order' => $request->input('order'),
            'icon' => $request->input('icon'),
        ]);
        return response()->json($data, 200);
    }

    public function update (Request $request) : JsonResponse {
        $data = $this->menuServices->update($request->input('id'), [
            'name' => $request->input('name'),
            'link' => $request->input('link'),
            'parent_id' => $request->input('parent_id'),
            'order' => $request->input('order'),
            'icon' => $request->input('icon'),
        ]);
        return response()->json($data, 200);
    }

    public function hapus (Request $request) : JsonResponse {
        $data = $this->menuServices->delete($request->input('id'));
        return response()->json($data, 200);
    }

    public function getChildMenu (Request $request) : JsonResponse {
        $data = $this->menuServices->getChildMenuByParentID($request->input('id_parent'));
        return $data;
    }

    public function getAllMenu () : JsonResponse {
        $data = $this->menuServices->getAllMenu();
        return response()->json($data);
    }
}
