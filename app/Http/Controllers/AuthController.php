<?php

namespace App\Http\Controllers;

use App\Helpers\SessionsHelper;
use App\Services\AuthServices;
use App\Services\RoleAcessServices;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    protected $authBL;
    protected $userRole;

    public function __construct (AuthServices $authBL, RoleAcessServices $userRole) {
        $this->authBL = $authBL;
        $this->userRole = $userRole;
    }

    public function login() : View {
        return View('login');
    }

    public function auth_login(Request $request) : RedirectResponse {
        $userLogin = $this->authBL->login($request->input('username'), Hash::make($request->input('password')));
        if ($userLogin != null) {
            if (Hash::check($request->input('password'), $userLogin->password)) {
                $data_role = $this->userRole->cekRoleUser($userLogin->id);
                if (count($data_role) == 1) {
                    SessionsHelper::setSessions([
                        'name' => $userLogin->name,
                        'email' => $userLogin->email,
                        'nik' => $userLogin->nik,
                        'id' => $userLogin->id,
                        'id_role' => $data_role[0]->role_id,
                        'role_name' => $data_role[0]->name_role,
                    ]);
                    return redirect('/dashboard');
                } else if (count($data_role) > 1) {
                    SessionsHelper::setSessions([
                        'name' => $userLogin->name,
                        'email' => $userLogin->email,
                        'nik' => $userLogin->nik,
                        'id' => $userLogin->id,
                        'id_role' => '',
                        'role_name' => ''
                    ]);
                    return redirect('/auth/choose_role');
                } else {
                    //tidak punya akses
                    SessionsHelper::createFlashMessage('Tidak Punya Akses !');
                }
            } else {
                //password salah
                SessionsHelper::createFlashMessage('Password Salah !');
            }
        } else {
            //tidak terdaftar
            SessionsHelper::createFlashMessage('User Tidak Teregister !');
        }
        return redirect('/');
    }

    public function choose_role () : View {
        $data['role'] = $this->userRole->cekRoleUser(SessionsHelper::getSessions()->id);
        return View('choose_role', $data);
    }

    public function select_role (Request $request) : RedirectResponse {
        $data = $request->input('role_id');
        $data_split = explode('|', $data);
        SessionsHelper::setRoleSessions([
            'id' => $data_split[0],
            'nama_role' => $data_split[1]
        ]);
        return redirect('/dashboard');
    }

    public function logout () : RedirectResponse {
        SessionsHelper::clearSessions();
        return redirect('/');
    }
}
