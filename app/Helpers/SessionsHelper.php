<?php

namespace App\Helpers;

use App\BuisnessLogic\RoleAccessBuisnessLogic;
use Illuminate\Support\Facades\Session;

class SessionsHelper
{
    public $id;
    public $name;
    public $nik;
    public $email;
    public $id_role;
    public $role_name;

    public function __construct()
    {
        $sessions = Session::get('dLogin');
        if ($sessions != null) {
            $this->id = $sessions['id'];
            $this->name = $sessions['name'];
            $this->nik = $sessions['nik'];
            $this->email = $sessions['email'];
            $this->id_role = $sessions['id_role'];
            $this->role_name = $sessions['role_name'];
        }
    }

    public static function getSessions() : SessionsHelper
    {
        $helper = new SessionsHelper();
        return $helper;
    }

    public static function setSessions (array $dLogin) : void {
        Session::put('dLogin', $dLogin);
    }

    public static function setRoleSessions (array $role) : void {
        $sessions = Session::get('dLogin');
        $sessions["id_role"] = $role['id'];
        $sessions["role_name"] = $role['nama_role'];
        Session::forget('dLogin');
        Session::put('dLogin', $sessions);
    }

    public static function clearSessions () : void {
        Session::forget('dLogin');
    }

    public static function createFlashMessage(string $message) : void {
        Session::flash('message', $message);
    }

    public static function accessRoleCheck (string $path) : bool {
        $sessions = Session::get('dLogin');
        if ($sessions == null) {
            return false;
        }
        $roleAccess = new RoleAccessBuisnessLogic();
        $access = $roleAccess->cekAccessRole('/'.$path, $sessions["id_role"]);
        return $access;
    }
}
