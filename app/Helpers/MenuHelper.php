<?php

namespace App\Helpers;

use App\BuisnessLogic\MenuBuisnessLogic;
use Illuminate\Database\Eloquent\Collection;

class MenuHelper
{
    private MenuBuisnessLogic $menuServices;
    /**
     * Create a new class instance.
     */
    public function __construct()
    {
        $this->menuServices = new MenuBuisnessLogic();
    }

    public function getMenu () : Collection
    {
        return $this->menuServices->getMenu(SessionsHelper::getSessions()->id_role);
    }

    public function getChildMenu (int $menu_id) : Collection
    {
        return $this->menuServices->getChildMenu(SessionsHelper::getSessions()->id_role, $menu_id);
    }
}
