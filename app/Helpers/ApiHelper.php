<?php

namespace App\Helpers;

use Exception;

class ApiHelper
{
    public bool $success;
    public $data;
    public string $message;
    public string $stack_trace;

    public static function CreateError (Exception $e) : ApiHelper {
        $result = new ApiHelper();
        $result->success = false;
        $result->data = [];
        $result->message = $e->getMessage();
        $result->stack_trace = $e->getTraceAsString();
        return $result;
    }

    public static function CreateResult ($success, $data, $message) : ApiHelper {
        $result = new ApiHelper();
        $result->success = $success;
        $result->data = $data;
        $result->message = $message;
        $result->stack_trace = "";
        return $result;
    }
}
