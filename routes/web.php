<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\System\MenuController;
use App\Http\Controllers\System\RoleAccessController;
use App\Http\Controllers\System\RoleController;
use App\Http\Controllers\System\UserController;
use App\Http\Controllers\System\UserRoleController;
use App\Http\Middleware\AuthenticateApps;
use App\Http\Middleware\GuestMiddleware;
use Illuminate\Support\Facades\Route;

//Route Transaction
require __DIR__ . '/web_trans.php';

Route::controller(AuthController::class)->group(function (){
    Route::get('/', 'login');
    Route::get('/auth/logout', 'logout');
    Route::get('/auth/register', 'register');
    Route::post('/auth/login', 'auth_login');
    Route::post('/auth/select_role', 'select_role');
    Route::get('/auth/choose_role', 'choose_role');
});

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware(GuestMiddleware::class);
Route::get('/system/user/ubah_password', [UserController::class, 'formChangePassword'])->middleware(GuestMiddleware::class);

Route::controller(MenuController::class)->middleware(AuthenticateApps::class)->group(function () {
    Route::get('/system/menu', 'index');
    Route::get('/system/menu/getMenu', 'getMenu');
    Route::get('/system/menu/getAllMenu', 'getAllMenu');
    Route::post('/system/menu/getChildMenu', 'getChildMenu');
    Route::post('/system/menu/', 'getDataTable');
    Route::post('/system/menu/simpan', 'simpan');
    Route::post('/system/menu/update', 'update');
    Route::post('/system/menu/hapus', 'hapus');
});

Route::controller(RoleAccessController::class)->middleware(AuthenticateApps::class)->group(function () {
    Route::post('/system/role_access', 'getDataTable');
    Route::post('/system/role_access/simpan', 'simpan');
    Route::post('/system/role_access/hapus', 'hapus');
});

Route::controller(RoleController::class)->middleware(AuthenticateApps::class)->group(function () {
    Route::get('/system/role', 'index');
    Route::get('/system/role/getRole', 'getRole');
    Route::get('/system/role/getAllRole', 'getAllRole');
    Route::post('/system/role', 'getDataTable');
    Route::post('/system/role/simpan', 'simpan');
    Route::post('/system/role/update', 'update');
    Route::post('/system/role/hapus', 'hapus');
})->middleware(AuthenticateApps::class);

Route::controller(UserRoleController::class)->middleware(AuthenticateApps::class)->group(function () {
    Route::post('/system/user_role/', 'getDataTable');
    Route::post('/system/user_role/simpan', 'simpan');
    Route::post('/system/user_role/hapus', 'hapus');
});

Route::controller(UserController::class)->middleware(AuthenticateApps::class)->group(function () {
    Route::get('/system/user', 'index');
    Route::get('/system/user/getUser', 'getUser');
    Route::post('/system/user', 'getDataTable');
    Route::post('/system/user/change_password', 'changeMyPassword');
    Route::post('/system/user/simpan', 'simpan');
    Route::post('/system/user/update', 'update');
    Route::post('/system/user/hapus', 'hapus');
    Route::post('/system/user/reset_password', 'resetPassword');
});
